FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Notifications.Abstractions /app/LendFoundry.Notifications.Abstractions
WORKDIR /app/LendFoundry.Notifications.Abstractions
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Notifications /app/LendFoundry.Notifications
WORKDIR /app/LendFoundry.Notifications
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Notifications.Templates.Repository /app/LendFoundry.Notifications.Templates.Repository
WORKDIR /app/LendFoundry.Notifications.Templates.Repository
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Notifications.Templates /app/LendFoundry.Notifications.Templates
WORKDIR /app/LendFoundry.Notifications.Templates
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Notifications.Channels.SendGrid /app/LendFoundry.Notifications.Channels.SendGrid
WORKDIR /app/LendFoundry.Notifications.Channels.SendGrid
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Notifications.Client /app/LendFoundry.Notifications.Client
WORKDIR /app/LendFoundry.Notifications.Client
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Notifications.Api /app/LendFoundry.Notifications.Api
WORKDIR /app/LendFoundry.Notifications.Api
RUN eval "$CMD_RESTORE"

RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel
