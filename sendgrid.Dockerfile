FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Notifications.Abstractions /app/LendFoundry.Notifications.Abstractions
WORKDIR /app/LendFoundry.Notifications.Abstractions
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Notifications.Channels.SendGrid /app/LendFoundry.Notifications.Channels.SendGrid
WORKDIR /app/LendFoundry.Notifications.Channels.SendGrid
RUN eval "$CMD_RESTORE"

ENTRYPOINT dnx run
