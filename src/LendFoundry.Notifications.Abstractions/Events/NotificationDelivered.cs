namespace LendFoundry.Notifications.Events
{
    public class NotificationDelivered : NotificationRequested
    {
        public NotificationDelivered()
        {
        }

        public NotificationDelivered(INotification notification)
        {
            Format = notification.Format;
            Recipient = notification.Recipient;
            Subject = notification.Subject;
            Body = notification.Body;
        }
    }
}