﻿namespace LendFoundry.Notifications.Events
{
    public class NotificationRequested : INotification
    {
        public NotificationRequested()
        {
        }

        public NotificationRequested(INotification notification)
        {
            Format = notification.Format;
            Recipient = notification.Recipient;
            Subject = notification.Subject;
            Body = notification.Body;
        }

        public MessageFormat Format { get; set; }
        public Recipient Recipient { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}