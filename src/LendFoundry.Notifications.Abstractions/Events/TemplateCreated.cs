﻿namespace LendFoundry.Notifications.Events
{
    public class TemplateCreated : TemplateEvent
    {
        public TemplateCreated(ITemplate template) : base(template)
        {

        }
    }
}
