﻿namespace LendFoundry.Notifications.Events
{
    public class TemplateDeleted : TemplateEvent
    {
        public TemplateDeleted(ITemplate template) : base(template)
        {
        }
    }
}
