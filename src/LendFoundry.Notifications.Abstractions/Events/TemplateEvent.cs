﻿namespace LendFoundry.Notifications.Events
{
    public class TemplateEvent
    {
        public TemplateEvent(ITemplate template)
        {
            Template = template;
        }

        public ITemplate Template { get; set; }
    }
}
