﻿namespace LendFoundry.Notifications.Events
{
    public class TemplateUpdated : TemplateEvent
    {
        public TemplateUpdated(ITemplate template) : base(template)
        {
        }
    }
}
