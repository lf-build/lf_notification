namespace LendFoundry.Notifications
{
    public interface IDeliveryChannel
    {
        void Start();
    }
}