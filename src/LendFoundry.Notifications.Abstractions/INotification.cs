namespace LendFoundry.Notifications
{
    public interface INotification
    {
        MessageFormat Format { get; set; }
        Recipient Recipient { get; set; }
        string Subject { get; set; }
        string Body { get; set; }
    }
}