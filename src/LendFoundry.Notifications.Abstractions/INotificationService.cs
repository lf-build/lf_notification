﻿using System.Collections.Generic;

namespace LendFoundry.Notifications
{
    public interface INotificationService
    {
        void SendNotification(ITemplate template, IEnumerable<NotificationRequest> requests);
    }
}
