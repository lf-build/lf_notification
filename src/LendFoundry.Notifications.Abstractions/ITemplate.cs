using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Notifications
{
    public interface ITemplate : IAggregate
    {
        string Key { get; set; }
        string Name { get; set; }
        string Subject { get; set; }
        string Body { get; set; }
        MessageFormat Format { get; set; }
    }
}