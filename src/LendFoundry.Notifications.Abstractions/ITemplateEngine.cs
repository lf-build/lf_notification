﻿namespace LendFoundry.Notifications
{
    public interface ITemplateEngine<T>
    {
        string Parse(string template, object data);
    }
}
