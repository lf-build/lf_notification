﻿using System.Collections.Generic;

namespace LendFoundry.Notifications
{
    public interface ITemplateService
    {
        ITemplate Add(NewTemplateRequest template);
        ITemplate Update(UpdateTemplateRequest request);
        ITemplate Get(string key, MessageFormat format);
        ITemplate Delete(string key, MessageFormat format);
        IEnumerable<ITemplate> All(string key);
        IEnumerable<ITemplate> All();
    }
}
