﻿namespace LendFoundry.Notifications
{
    public class InvalidTemplateException : TemplateException
    {
        public InvalidTemplateException(string message) : base(message)
        {
        }
    }
}
