namespace LendFoundry.Notifications
{
    /// <summary>
    /// Enum to specify the format in which a message has to be sent.
    /// </summary>
    public enum MessageFormat
    {
        Unspecified,
        Text,
        Html
    }
}
