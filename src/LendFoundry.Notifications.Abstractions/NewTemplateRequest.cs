﻿namespace LendFoundry.Notifications
{
    public class NewTemplateRequest
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public MessageFormat Format { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(Key))
                throw new InvalidTemplateException("Template key cannot be empty.");

            if (string.IsNullOrWhiteSpace(Name))
                throw new InvalidTemplateException("Template name cannot be empty.");

            if (string.IsNullOrWhiteSpace(Subject))
                throw new InvalidTemplateException("Template subject cannot be empty.");

            if (string.IsNullOrWhiteSpace(Body))
                throw new InvalidTemplateException("Template body cannot be empty.");

            if (Format == MessageFormat.Unspecified)
                throw new InvalidTemplateException("Template format is invalid.");
        }
    }
}