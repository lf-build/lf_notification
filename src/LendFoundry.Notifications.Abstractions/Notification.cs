namespace LendFoundry.Notifications
{
    public class Notification : INotification
    {
        public MessageFormat Format { get; set; }
        public Recipient Recipient { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}