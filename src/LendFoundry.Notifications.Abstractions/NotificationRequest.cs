﻿namespace LendFoundry.Notifications
{
    public class NotificationRequest
    {
        public Recipient Recipient { get; set; }
        public object Data { get; set; }
    }
}
