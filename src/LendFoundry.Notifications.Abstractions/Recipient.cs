﻿namespace LendFoundry.Notifications
{
    public class Recipient
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string LoanReferenceNumber { get; set; }
    }
}