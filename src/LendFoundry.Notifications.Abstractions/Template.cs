﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Notifications
{
    public class Template : Aggregate, ITemplate
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }  
        public MessageFormat Format { get; set; }
    }
}