﻿using System;

namespace LendFoundry.Notifications
{
    public class TemplateException : Exception
    {
        public TemplateException(string message) : base(message)
        {
        }
    }
}
