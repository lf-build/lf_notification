﻿namespace LendFoundry.Notifications
{
    public class TemplateNotFoundException : TemplateException
    {
        public TemplateNotFoundException(string message) : base(message)
        {
        }
    }
}
