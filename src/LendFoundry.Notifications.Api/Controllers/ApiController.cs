﻿using LendFoundry.Foundation.Services;
using LendFoundry.Notifications.Api.ViewModels;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Notifications.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(
            ITemplateService templateService, 
            ITemplateEngine<dynamic> templateEngine, 
            INotificationService notificationService, 
            ILogger logger) 
            :base(logger) 
        {
            TemplateService = templateService;
            NotificationService = notificationService;
            TemplateEngine = templateEngine;
        }
        
        private INotificationService NotificationService { get; }

        private ITemplateService TemplateService { get; }

        private ITemplateEngine<dynamic> TemplateEngine { get; }

        [HttpPut("/"), HttpPost("/")]
        public IActionResult CreateTemplate([FromBody] NewTemplateRequest request)
        {
            return Execute(() =>
            {
                try
                {
                    var template = TemplateService.Add(request);
                    return new HttpOkObjectResult(TemplateViewModel.From(template));
                }
                catch (TemplateException ex)
                {
                    throw new InvalidArgumentException(ex.Message);
                }
            });
        }

        [HttpPost("/{key}/{format}")]
        public IActionResult UpdateTemplate([FromRoute] string key, [FromRoute] MessageFormat format, [FromBody] UpdateTemplateRequest request)
        {
            if (request != null)
            {
                request.Key = key;
                request.Format = format;
            }

            return Execute(() =>
            {
                try
                {
                    var template = TemplateService.Update(request);
                    return new HttpOkObjectResult(TemplateViewModel.From(template));
                }
                catch (TemplateException ex)
                {
                    throw new InvalidArgumentException(ex.Message);
                }
            });
        }

        [HttpDelete("/{key}/{format}")]
        public IActionResult DeleteTemplate([FromRoute] string key, MessageFormat format)
        {
            return Execute(() =>
            {
                try
                {
                    var template = TemplateService.Delete(key, format);
                    return new HttpOkObjectResult(TemplateViewModel.From(template));
                }
                catch (TemplateException ex)
                {
                    throw new InvalidArgumentException(ex.Message);
                }
            });
        }

        [HttpGet("/{key}")]
        public IActionResult GetTemplate([FromRoute]string key)
        {
            return Execute(() =>
            {
                try
                {
                    var templates = TemplateService.All(key);
                    return new HttpOkObjectResult(TemplateViewModel.From(templates));
                }
                catch (TemplateException ex)
                {
                    throw new NotFoundException(ex.Message);
                }
            });
        }

        [HttpGet("/{key}/{format}")]
        public IActionResult GetTemplate([FromRoute]string key, [FromRoute] MessageFormat format)
        {
            return Execute(() =>
            {
                try
                {
                    var template = TemplateService.Get(key, format);
                    return new ObjectResult(TemplateViewModel.From(template));
                }
                catch (TemplateException ex)
                {
                    throw new NotFoundException(ex.Message);
                }
            });
        }

        [HttpGet("/")]
        public IActionResult GetAllTemplates()
        {
            return Execute(() =>
            {
                try
                {
                    var templates = TemplateService.All();
                    return new HttpOkObjectResult(TemplateViewModel.From(templates));
                }
                catch (TemplateException ex)
                {
                    throw new NotFoundException(ex.Message);
                }
            });
        }

        [HttpPost]
        [Route("/{key}/{format}/test")]
        public IActionResult TestTemplateBinding(string key, MessageFormat format, [FromBody] object data)
        {
            if (data == null)
                return ErrorResult.BadRequest("Template data is empty or invalid.");

            return Execute(() =>
            {
                var template = TemplateService.Get(key, format);
                return new ObjectResult(new
                {
                    Subject = TemplateEngine.Parse(template.Subject, data),
                    Body = TemplateEngine.Parse(template.Body, data)
                });
            });
        }

        [HttpPost]
        [Route("/{key}/{format}/send")]
        public IActionResult SendNotification(string key, MessageFormat format, [FromBody] List<NotificationRequest> requests)
        {
            if (requests == null || !requests.Any())
                return ErrorResult.BadRequest("The recipients list is empty or its format is invalid");

            return Execute(() =>
            {
                try
                {
                   var template = TemplateService.Get(key, format);
                    NotificationService.SendNotification(template, requests);
                    return NoContent();
                }
                catch (TemplateException ex)
                {
                    throw new NotFoundException(ex.Message);
                }
            });
        }

        [HttpPost]
        [Route("/{key}/send")]
        public IActionResult SendNotification([FromRoute] string key, [FromBody] List<NotificationRequest> requests)
        {
            if (requests == null || !requests.Any())
                return ErrorResult.BadRequest("The recipients list is empty or its format is invalid");

            return Execute(() =>
            {
                var templates = TemplateService.All(key).ToList();

                if (!templates.Any())
                    return new ErrorResult(404, $"No templates found with key={key}");

                Parallel.ForEach(templates, template =>
                {
                    NotificationService.SendNotification(template, requests);
                });

                return NoContent();
            });
        }

        private static IActionResult NoContent()
        {
            return new HttpStatusCodeResult(204);
        }
    }
}