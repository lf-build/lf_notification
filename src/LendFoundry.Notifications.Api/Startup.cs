﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Notifications.Templates;
using LendFoundry.Notifications.Templates.Repository;
using LendFoundry.Security.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Notifications.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);
            services.AddConfigurationService<NotificationConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, "notifications");
            services.AddTransient(p => p.GetService<IConfigurationService<NotificationConfiguration>>().Get());
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<ITemplateService, TemplateService>();
            services.AddTransient<ITemplateRepository, MongoTemplateRepository>();
            services.AddTransient<ITemplateEngine<dynamic>, HandleBarsTemplateEngine<dynamic>>();
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseSecurityControl();
            app.UseRequestLogging();
            app.UseMvc();
        }
    }
}