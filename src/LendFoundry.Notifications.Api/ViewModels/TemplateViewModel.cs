﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Notifications.Api.ViewModels
{
    public class TemplateViewModel
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public MessageFormat Format { get; set; }

        public static IEnumerable<TemplateViewModel> From(IEnumerable<ITemplate> templates)
        {
            if (templates == null)
                throw new ArgumentNullException(nameof(templates));

            return templates.Select(template => From(template));
        }

        public static TemplateViewModel From(ITemplate template)
        {
            if (template == null)
                throw new ArgumentNullException(nameof(template));

            return new TemplateViewModel
            {
                Key = template.Key,
                Name = template.Name,
                Subject = template.Subject,
                Body = template.Body,
                Format = template.Format
            };
        }
    }
}
