namespace LendFoundry.Notifications.Channels.SendGrid
{
    public class Configuration
    {
        public string ApiKey { get; set; }
        public string FromName { get; set; }
        public string FromAddress { get; set; }
    }
}