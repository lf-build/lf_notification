﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Notifications.Channels.SendGrid
{
    public class Program : DependencyInjection
    {
        public void Main(string[] args)
        {
            Provider.GetRequiredService<IDeliveryChannel>().Start();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddSingleton<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddTransient<IDeliveryChannel, SendGridDeliveryChannel>();
            services.AddSingleton<ITokenReader, TransientTokenReader>();
            return services;
        }
    }
}
