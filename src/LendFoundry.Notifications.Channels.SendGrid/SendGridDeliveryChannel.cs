﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Notifications.Events;
using LendFoundry.Security.Tokens;
using SendGrid;
using System.Net.Mail;
using System.Threading.Tasks;
using System;

namespace LendFoundry.Notifications.Channels.SendGrid
{
    public class SendGridDeliveryChannel : IDeliveryChannel
    {
        public SendGridDeliveryChannel(IEventHubClient eventHub, IConfigurationServiceFactory<Configuration> configurationFactory, ITokenHandler tokenHandler, ILogger logger)
        {
            EventHub = eventHub;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            Logger = logger;
        }

        private IEventHubClient EventHub { get; }

        private IConfigurationServiceFactory<Configuration> ConfigurationFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILogger Logger { get; }

        public void Start()
        {
            Logger.Info("Started SendGrid listener...");
            try
            {
                Logger.Info($"The SendGrid listener is waiting a {nameof(NotificationRequested)} event");
                EventHub.On(nameof(NotificationRequested), async @event =>
                {
                    Logger.Info($"Attached eventhub subscription for #{@event.Name}");
                    var notification = GetEventData(@event);

                    if (notification == null)
                    {
                        Logger.Error("Notification must to be informed");
                        return;
                    }

                    try
                    {
                        Logger.Info("Notification requested", notification);
                        await SendNotification(@event, notification);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error("Error trying to send notification", exception, notification);
                    }
                });

                EventHub.Start();
            }
            catch (WebSocketSharp.WebSocketException ex)
            {
                Logger.Error("Error while listening eventhub to process 'SendGrid'. Error: ", ex);
                Logger.Info("Trying to reset service...\n");
                Start();
            }
            catch (Exception ex)
            {
                Logger.Error("Error while listening eventhub to process 'SendGrid'. Error: ", ex);
                Logger.Info("Trying to reset service...\n");
                Start();
            }
        }

        private NotificationRequested GetEventData(EventInfo @event)
        {
            try
            {
                return @event.Cast<NotificationRequested>();
            }
            catch (Exception exception)
            {
                Logger.Error("Error trying to parse event data", exception);
                return null;
            }
        }

        private async Task SendNotification(EventInfo @event, NotificationRequested notification)
        {
            var token = TokenHandler.Issue(@event.TenantId ?? Settings.Tenant, Settings.Issuer);
            var configService = ConfigurationFactory.Create(new StaticTokenReader(token.Value));
            var config = configService.Get();
            var recipient = notification.Recipient;

            if (string.IsNullOrWhiteSpace(recipient.Name))
            {
                Logger.Warn("Recipient's name is empty");
            }

            if (string.IsNullOrWhiteSpace(recipient.Email))
            {
                Logger.Error("Notification cannot be sent: recipient's email address is empty");
                return;
            }

            var message = new SendGridMessage
            {
                From = new MailAddress(config.FromAddress, config.FromName),
                To = new[] { new MailAddress(recipient.Email, recipient.Name) },
                Subject = notification.Subject,
                Html = notification.Body
            };

            var transport = new Web(config.ApiKey);
            await transport.DeliverAsync(message).ContinueWith(t =>
            {
                if (t.Status == TaskStatus.Faulted)
                    Logger.Error("Error sending notification via SendGrid", t.Exception);
            })
            .ContinueWith(t =>
            {
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    Logger.Info("Notification sent");

                    EventHub.Publish(nameof(NotificationDelivered), new NotificationDelivered(notification));
                    Logger.Info($"The event {nameof(NotificationDelivered)} was published");
                }
            });
        }
    }
}