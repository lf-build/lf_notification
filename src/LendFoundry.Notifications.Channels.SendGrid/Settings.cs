﻿using System;
using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Notifications.Channels.SendGrid
{
    public static class Settings
    {
        public static string ServiceName { get; } = "notifications-sendgrid";

        public static ServiceSettings Configuration { get; } = new ServiceSettings("SENDGRID_CONFIGURATION_HOST", "configuration", "SENDGRID_CONFIGURATION_PORT", 5000);

        public static ServiceSettings EventHub { get; } = new ServiceSettings("SENDGRID_EVENTHUB_HOST", "eventhub", "SENDGRID_EVENTHUB_PORT", 5000);

        public static string Tenant => Environment.GetEnvironmentVariable("SENDGRID_TOKEN_TENANT") ?? "lendfoundry";

        public static string Issuer => Environment.GetEnvironmentVariable("SENDGRID_TOKEN_ISSUER") ?? "lendfoundry";
        
    }
}