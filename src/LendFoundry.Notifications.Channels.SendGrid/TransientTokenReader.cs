using LendFoundry.Security.Tokens;

namespace LendFoundry.Notifications.Channels.SendGrid
{
    public class TransientTokenReader : ITokenReader
    {
        public string Tenant { get; set; }

        public string Read()
        {
            return new TokenHandler(null, null, null).Issue(Tenant ?? Settings.Tenant, Settings.Issuer).Value;
        }
    }
}