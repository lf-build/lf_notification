﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Notifications.Client
{
    public interface INotificationService
    {
        Task<bool> Send(string templateKey, MessageFormat format, IEnumerable<NotificationRequest> recipients);
    }
}