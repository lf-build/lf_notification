﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Notifications.Client
{
    public interface INotificationServiceFactory
    {
        INotificationService Create(ITokenReader reader);
    }
}