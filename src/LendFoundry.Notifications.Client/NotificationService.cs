﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using RestSharp;
using System.Linq;

namespace LendFoundry.Notifications.Client
{
    public class NotificationService : INotificationService
    {
        public NotificationService(IServiceClient client)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<bool> Send(string templateKey, MessageFormat format, IEnumerable<NotificationRequest> recipients)
        {
            if (templateKey == null) throw new ArgumentNullException(nameof(templateKey));
            if (string.IsNullOrWhiteSpace(templateKey)) throw new ArgumentException("Argument cannot be empty", nameof(templateKey));
            if (recipients == null) throw new ArgumentNullException(nameof(recipients));
            if (!recipients.Any()) throw new ArgumentException("Recipients list cannot be empty", nameof(recipients));

            var request = new RestRequest("/{key}/{format}/send", Method.POST);
            request.AddUrlSegment("key", templateKey);
            request.AddUrlSegment("format", format.ToString());
            request.AddJsonBody(recipients);
            return await Client.ExecuteAsync(request);
        }
    }
}
