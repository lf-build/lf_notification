using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Notifications.Client
{
    public static class NotificationServiceExtensions
    {
        public static IServiceCollection AddNotificationService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<INotificationServiceFactory>(p => new NotificationServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<INotificationServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}