﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Notifications.Templates.Repository
{
    public interface ITemplateRepository : IRepository<ITemplate>
    {
        IEnumerable<ITemplate> All();
        IEnumerable<ITemplate> All(string key);
        bool Contains(string key, MessageFormat format);
        ITemplate Get(string key, MessageFormat format);
    }
}
