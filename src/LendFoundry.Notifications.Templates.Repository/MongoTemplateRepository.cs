﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System;

namespace LendFoundry.Notifications.Templates.Repository
{
    public class MongoTemplateRepository : MongoRepository<ITemplate, Template>, ITemplateRepository
    {
        public MongoTemplateRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "templates")
        {
        }

        public IEnumerable<ITemplate> All()
        {
            return Query.ToList();
        }

        public IEnumerable<ITemplate> All(string key)
        {
            return Query
                .Where(t => t.Key == key)
                .ToList();
        }

        public ITemplate Get(string key, MessageFormat format)
        {
            return GetQuery(key, format).SingleOrDefault();
        }

        public bool Contains(string key, MessageFormat format)
        {
            return GetQuery(key, format).Any();
        }

        private IMongoQueryable<ITemplate> GetQuery(string key, MessageFormat format)
        {
            return Query
                .Where(t => t.Key == key)
                .Where(t => t.Format == format);
        }
    }
}
