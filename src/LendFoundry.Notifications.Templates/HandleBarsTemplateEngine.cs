﻿using HandlebarsDotNet;
using System;

namespace LendFoundry.Notifications.Templates
{
    public class HandleBarsTemplateEngine<TModel> : ITemplateEngine<TModel>
    {
        public string Parse(string template, object data)
        {
            if (template == null)
                throw new ArgumentNullException(nameof(template));

            if (data == null)
                throw new ArgumentNullException(nameof(data));

            return Handlebars.Compile(template)(data);
        }
    }
}
