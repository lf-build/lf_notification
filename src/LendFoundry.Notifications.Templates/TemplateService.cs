﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Notifications.Events;
using LendFoundry.Notifications.Templates.Repository;
using System.Collections.Generic;
using System;

namespace LendFoundry.Notifications.Templates
{
    public class TemplateService : ITemplateService
    {
        public TemplateService(ITemplateRepository repository, IEventHubClient eventHub, ILogger logger)
        {
            Repository = repository;
            EventHub = eventHub;
            Logger = logger;
        }

        private ITemplateRepository Repository { get; }

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }

        public ITemplate Add(NewTemplateRequest request)
        {
            try
            {
                if (request == null)
                    throw new InvalidTemplateException("Request cannot be null");

                request.Validate();

                AssertTemplateDoesNotExist(request);

                var template = new Template
                {
                    Key = request.Key,
                    Name = request.Name,
                    Subject = request.Subject,
                    Body = request.Body,
                    Format = request.Format
                };

                Repository.Add(template);

                EventHub.Publish("NotificationTemplateCreated", new TemplateCreated(template));
                Logger.Info($"The {nameof(TemplateCreated)} event was published named (NotificationTemplateCreated)");

                return template;
            }
            catch (Exception ex)
            {
                Logger.Error("The unhandled exception occurred on the 'Add'. Error: ", ex);
                throw;
            }
        }

        public ITemplate Update(UpdateTemplateRequest request)
        {
            if (request == null)
                throw new TemplateException("Request cannot be null");

            request.Validate();

            AssertTemplateExists(request);

            var template = Get(request.Key, request.Format);
            template.Name = request.Name;
            template.Subject = request.Subject;
            template.Body = request.Body;

            Repository.Update(template);

            EventHub.Publish("NotificationTemplateUpdated", new TemplateUpdated(template));
            Logger.Info($"The {nameof(TemplateUpdated)} event was published named (NotificationTemplateUpdated)");

            return template;
        }

        public IEnumerable<ITemplate> All()
        {
            return Repository.All();
        }

        public IEnumerable<ITemplate> All(string key)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(key))
                    throw new InvalidTemplateException("Template key cannot be empty.");

                return Repository.All(key);
            }
            catch (Exception ex)
            {
                Logger.Error("The unhandled exception occurred on the 'GetTemplates'. Error: ", ex);
                throw;
            }
        }

        public ITemplate Delete(string key, MessageFormat format)
        {
            var template = Get(key, format);
            Repository.Remove(template);

            EventHub.Publish("NotificationTemplateDeleted", new TemplateDeleted(template));
            Logger.Info($"The {nameof(TemplateDeleted)} event was published named (NotificationTemplateDeleted)");

            return template;
        }

        public ITemplate Get(string key, MessageFormat format)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(key))
                    throw new InvalidTemplateException("Template key cannot be empty.");

                if (format == MessageFormat.Unspecified)
                    throw new InvalidTemplateException("Template format is invalid");

                var template = Repository.Get(key, format);

                if (template == null)
                    throw new TemplateNotFoundException($"Template not found with key='{key}' and format='{format}'");

                return template;
            }
            catch (Exception ex)
            {
                Logger.Error("The unhandled exception occurred on the 'Get'. Error: ", ex);
                throw;
            }
        }

        private void AssertTemplateExists(UpdateTemplateRequest request)
        {
            try
            {
                if (!Repository.Contains(request.Key, request.Format))
                    throw new TemplateNotFoundException($"Template not found with key='{request.Key}' and format='{request.Format}");
            }
            catch (Exception ex)
            {
                Logger.Error("The unhandled exception occurred on the 'Update'. Error: ", ex);
                throw;
            }
        }

        private void AssertTemplateDoesNotExist(NewTemplateRequest request)
        {
            try
            {
                if (Repository.Contains(request.Key, request.Format))
                    throw new InvalidTemplateException($"A template already exists with key={request.Key} and format={request.Format}");
            }
            catch (Exception ex)
            {
                Logger.Error("The unhandled exception occurred on the 'Add'. Error: ", ex);
                throw;
            }
        }
    }
}