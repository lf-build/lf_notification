﻿using System;
using LendFoundry.EventHub.Client;
using LendFoundry.Notifications.Events;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;

namespace LendFoundry.Notifications
{
    public class NotificationService : INotificationService
    {
        public NotificationService(
            ITemplateEngine<dynamic> templateEngine, 
            IEventHubClientFactory eventHubFactory, 
            ITenantService tenantService, 
            ITokenHandler tokenHandler, 
            NotificationConfiguration configuration,
            ILogger logger)
        {
            TemplateEngine = templateEngine;
            EventHubFactory = eventHubFactory;
            TenantService = tenantService;
            TokenHandler = tokenHandler;
            Configuration = configuration;
            Logger = logger;
        }

        private ITenantService TenantService { get; }

        private ITemplateEngine<dynamic> TemplateEngine { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private NotificationConfiguration Configuration { get; }

        private ILogger Logger { get; }


        public void SendNotification(ITemplate template, IEnumerable<NotificationRequest> requests)
        {
            try
            {
                var token = TokenHandler.Issue(TenantService.Current.Id, "notifications");
                var tokenReader = new StaticTokenReader(token.Value);
                var eventHub = EventHubFactory.Create(tokenReader);

                Parallel.ForEach(requests, async request =>
                {
                    await eventHub.Publish(nameof(NotificationRequested),
                        new NotificationRequested(CreateNotification(template, request)));
                });
                Logger.Info($"The {nameof(NotificationRequested)} event was published.");
            }
            catch (Exception ex)
            {
                Logger.Error("The Unhandled exception occurred when [SendNotification] was called. Error: ", ex);
                throw;
            }
        }

        private Notification CreateNotification(ITemplate template, NotificationRequest request)
        {
            try
            {
                var subject = TemplateEngine.Parse(template.Subject, request.Data);
                var body = ParseNotificationBody(template.Body, request.Data);
                return new Notification
                {
                    Recipient = request.Recipient,
                    Subject = subject,
                    Body = body,
                    Format = template.Format
                };
            }
            catch (Exception ex)
            {
                Logger.Error("The Unhandled exception occurred when [CreateNotification] was called. Error: ", ex);
                throw;
            }
        }

        private string ParseNotificationBody(string templateBody, object data)
        {
            var contents = TemplateEngine.Parse(templateBody, data);

            if (Configuration == null)
                throw new InvalidArgumentException($"The {nameof(NotificationConfiguration)} must to be informed!");

            if (string.IsNullOrWhiteSpace(Configuration.MasterTemplate))
                return contents;

            return TemplateEngine.Parse(Configuration.MasterTemplate, new
            {
                Contents = contents
            });
        }
    }
}