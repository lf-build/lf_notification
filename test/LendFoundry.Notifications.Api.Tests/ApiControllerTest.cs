﻿using LendFoundry.Notifications.Api.Controllers;
using LendFoundry.Notifications.Api.ViewModels;
using LendFoundry.Notifications.Templates;
using Microsoft.AspNet.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Logging;
using Xunit;

namespace LendFoundry.Notifications.Api.Tests
{
    public class ApiControllerTest
    {
        private ApiController Controller { get; }
        private Mock<ITemplateService> TemplateServiceMock { get; } = new Mock<ITemplateService>();

        public ApiControllerTest()
        {
            Controller = new ApiController(
                TemplateServiceMock.Object, 
                new HandleBarsTemplateEngine<dynamic>(), 
                Mock.Of<INotificationService>(),
                Mock.Of<ILogger>()
                );
        }

        [Fact]
        public void TestCreateTemplateWithSimpleTemplate()
        {
            var newTemplateRequest = new NewTemplateRequest();
            var expectedTemplate = Mock.Of<ITemplate>(t =>
                t.Key == "upcoming-payment" &&
                t.Name == "Upcoming Payment" &&
                t.Subject == "Your payment is coming up" &&
                t.Body == "Make sure there's money in your account" &&
                t.Format == MessageFormat.Html
            );

            TemplateServiceMock
                .Setup(s => s.Add(newTemplateRequest))
                .Returns(expectedTemplate);

            var result = (ObjectResult)Controller.CreateTemplate(newTemplateRequest);
            Assert.NotNull(result);

            var template = (TemplateViewModel)result.Value;
            Assert.NotNull(template);
            Assert.Equal("upcoming-payment", template.Key);
            Assert.Equal("Upcoming Payment", template.Name);
            Assert.Equal("Your payment is coming up", template.Subject);
            Assert.Equal("Make sure there's money in your account", template.Body);
            Assert.Equal(MessageFormat.Html, template.Format);
        }

        [Fact]
        public void TestUpdateTemplate()
        {
            var updateTemplateRequest = new UpdateTemplateRequest
            {
                Name = "Test Template",
                Subject = "Template for testing purposes",
                Body = "<html>This is the changed body of the template.</html>"
            };

            var updatedTemplate = Mock.Of<ITemplate>(t =>
                t.Key == "test" &&
                t.Name == "Test Template" &&
                t.Subject == "Template for testing purposes" &&
                t.Body == "<html>This is the changed body of the template.</html>" &&
                t.Format == MessageFormat.Html
            );

            TemplateServiceMock
                .Setup(s => s.Update(updateTemplateRequest))
                .Returns(updatedTemplate);

            var result = (HttpOkObjectResult)Controller.UpdateTemplate("test", MessageFormat.Html, updateTemplateRequest);
            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);

            var template = (TemplateViewModel)result.Value;
            Assert.NotNull(template);
            Assert.Equal("test", template.Key);
            Assert.Equal("Test Template", template.Name);
            Assert.Equal("Template for testing purposes", template.Subject);
            Assert.Equal("<html>This is the changed body of the template.</html>", template.Body);
            Assert.Equal(MessageFormat.Html, template.Format);
        }

        [Fact]
        public void TestGetTemplate()
        {
            var expectedTemplate = Mock.Of<ITemplate>(t =>
                t.Key == "upcoming-payment" &&
                t.Name == "Upcoming Payment" &&
                t.Subject == "Your payment is coming up" &&
                t.Body == "Make sure there's money in your account" &&
                t.Format == MessageFormat.Html
            );

            TemplateServiceMock
                .Setup(s => s.Get("upcoming-payment", MessageFormat.Html))
                .Returns(expectedTemplate);

            var result = (ObjectResult)Controller.GetTemplate("upcoming-payment", MessageFormat.Html);
            Assert.NotNull(result);
            var template = (TemplateViewModel)result.Value;
            Assert.NotNull(template);
            Assert.Equal("upcoming-payment", template.Key);
            Assert.Equal("Upcoming Payment", template.Name);
            Assert.Equal("Your payment is coming up", template.Subject);
            Assert.Equal("Make sure there's money in your account", template.Body);
            Assert.Equal(MessageFormat.Html, template.Format);
        }

        [Fact]
        public void TestGetAllTemplates()
        {
            var expectedTemplates = new List<ITemplate> {
                Mock.Of<ITemplate>(t =>
                    t.Key == "tmpl01" &&
                    t.Name == "Template #1" &&
                    t.Subject == "This is template #1" &&
                    t.Body == "Contents of template #1" &&
                    t.Format == MessageFormat.Html),

                Mock.Of<ITemplate>(t =>
                    t.Key == "tmpl02" &&
                    t.Name == "Template #2" &&
                    t.Subject == "This is template #2" &&
                    t.Body == "Contents of template #2" &&
                    t.Format == MessageFormat.Text)
            };

            TemplateServiceMock
                .Setup(s => s.All())
                .Returns(expectedTemplates);

            var result = (ObjectResult)Controller.GetAllTemplates();
            Assert.NotNull(result);
            var templates = (IEnumerable<TemplateViewModel>)result.Value;
            Assert.NotNull(templates);
            Assert.Equal(2, templates.Count());

            var template1 = templates.ToList()[0];
            Assert.NotNull(template1);
            Assert.Equal("tmpl01", template1.Key);
            Assert.Equal("Template #1", template1.Name);
            Assert.Equal("This is template #1", template1.Subject);
            Assert.Equal("Contents of template #1", template1.Body);
            Assert.Equal(MessageFormat.Html, template1.Format);

            var template2 = templates.ToList()[1];
            Assert.NotNull(template2);
            Assert.Equal("tmpl02", template2.Key);
            Assert.Equal("Template #2", template2.Name);
            Assert.Equal("This is template #2", template2.Subject);
            Assert.Equal("Contents of template #2", template2.Body);
            Assert.Equal(MessageFormat.Text, template2.Format);
        }

        [Fact]
        public void TestDeleteTemplate()
        {
            var deletedTemplate = Mock.Of<ITemplate>(t =>
                t.Key == "welcome" &&
                t.Name == "Welcome Email" &&
                t.Subject == "Welcome to LendFoundry" &&
                t.Body == "Your loan has been approved" &&
                t.Format == MessageFormat.Html
            );

            TemplateServiceMock
                .Setup(s => s.Delete("welcome", MessageFormat.Html))
                .Returns(deletedTemplate);

            var result = (HttpOkObjectResult)Controller.DeleteTemplate("welcome", MessageFormat.Html);
            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);

            var template = (TemplateViewModel)result.Value;
            Assert.NotNull(template);
            Assert.Equal("welcome", template.Key);
            Assert.Equal("Welcome Email", template.Name);
            Assert.Equal("Welcome to LendFoundry", template.Subject);
            Assert.Equal("Your loan has been approved", template.Body);
            Assert.Equal(MessageFormat.Html, template.Format);
        }

        [Fact]
        public void TestTemplateBinding()
        {
            var expectedTemplate = Mock.Of<ITemplate>(t =>
                t.Subject == "Loan Of {{LoanAmount}} Approved for {{CustomerName}}" &&
                t.Body == "Hello, {{CustomerName}}. You are approved for a loan of {{LoanAmount}}"
            );

            TemplateServiceMock
                .Setup(s => s.Get("loan-approved", MessageFormat.Html))
                .Returns(expectedTemplate);

            var data = new { CustomerName = "Bill Gates", LoanAmount = 50000 };

            var response = (ObjectResult)Controller.TestTemplateBinding("loan-approved", MessageFormat.Html, data);
            Assert.NotNull(response);

            var result = response.Value;
            Assert.NotNull(result);
            Assert.Equal("Loan Of 50000 Approved for Bill Gates", ValueOf<string>("Subject", result));
            Assert.Equal("Hello, Bill Gates. You are approved for a loan of 50000", ValueOf<string>("Body", result));
        }

        private T ValueOf<T>(string propertyName, object obj)
        {
            return (T)obj.GetType().GetProperty(propertyName).GetValue(obj);
        }
    }
}