﻿using LendFoundry.Notifications.Catalog;
using System;
using Xunit;

namespace LendFoundry.Notifications.Catalog.Tests
{
    public class CatalogHelperTests
    {
        #region Id generation tests
        [Fact]
        public void TestIdGenerationWithSimpleName()
        {
            string name = "TestTemplateName";
            string id = CatalogHelper.GenerateIdFromName(name);
            Assert.Equal<string>("testtemplatename", id);
        }

        [Fact]
        public void TestIdGenerationWithNameHavingSpaces()
        {
            string name = "Test Template Name";
            string id = CatalogHelper.GenerateIdFromName(name);
            Assert.Equal<string>("test-template-name", id);
        }

        [Fact]
        public void TestIdGenerationWithNameHavingMultipleSpaces()
        {
            string name = "  Test       Template                       Name  ";
            string id = CatalogHelper.GenerateIdFromName(name);
            Assert.Equal<string>("test-template-name", id);
        }

        [Fact]
        public void TestIdGenerationWithNameHavingDashes()
        {
            string name = "  Test  -     -- Template  -              Name  ";
            string id = CatalogHelper.GenerateIdFromName(name);
            Assert.Equal<string>("test-template-name", id);
        }
        #endregion Id generation tests
    }
}