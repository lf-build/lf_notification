﻿using LendFoundry.Notifications.Core.Configuration;
using LendFoundry.Notifications.Core.Models;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using LendFoundry.Notifications.Abstractions;
using Xunit;

namespace LendFoundry.Notifications.Catalog.Tests
{
    public class CatalogTests
    {
        private Mock<ICatalog> CatalogMock { get; }
        private Mock<NotificationsDbContext> ContextMock { get; }
        private Mock<ISettings> SettingsMock { get; set; }
        private IDbSet<Template> DbSetMock { get; set; }

        public CatalogTests()
        {
            CatalogMock = new Mock<ICatalog>();
            ContextMock = new Mock<NotificationsDbContext>("Server=none;Database=none;Uid=none;Pwd=none");
            InitDbSetMock();
            SettingsMock = new Mock<ISettings>();
        }

        private void InitDbSetMock()
        {
            var template1 = new Template { Name = "Test Template 1", TenantId = "sometenant", Id = "test-template-1", Format = MessageFormat.Html, Body = "<html>Hello, {{CustomerName}}</html>" };
            var template2 = new Template { Name = "Test Template 2", TenantId = "sometenant", Id = "test-template-2", Format = MessageFormat.Html, Body = "<html>{{CustomerName}} is the second customer</html>" };
            var template3 = new Template { Name = "Test Template 3", TenantId = "sometenant", Id = "test-template-3", Format = MessageFormat.Text, Body = "This is a text template. The customer is {{CustomerName}}"};

            var apps = new List<Template>() { template1, template2, template3 }.AsQueryable();
            DbSetMock = Mock.Of<IDbSet<Template>>(set =>
                set.Provider == apps.Provider &&
                set.Expression == apps.Expression &&
                set.ElementType == apps.ElementType &&
                set.GetEnumerator() == apps.GetEnumerator());
        }

        [Fact(Skip = "Needs review...")]
        public void When_Template_Created_Then_Success()
        {
            //var template = new Template();
            //template.Name = "New Test Template";
            //template.TenantId = "sometenant";

            //var dbSetMock = new Mock<IDbSet<Template>>();
            //ContextMock.Setup(c => c.Templates).Returns(dbSetMock.Object);
            //CatalogMock.Setup(c => c.Add(template)).Returns("sometenant:new-test-template");


        }
    }
}
