﻿using LendFoundry.Notifications.Abstractions;
using LendFoundry.Notifications.Core.Models;
using LendFoundry.Notifications.Tests.Common;
using Xunit;

namespace LendFoundry.Notifications.Catalog.Tests
{
    public class TemplateCreationTests
    {
        public ICatalog Catalog { get; set; }

        public TemplateCreationTests()
        {
            Catalog = DataCreator.SetupTemplateCatalog();
        }

        [Fact]
        public void ShouldCreateTemplateAndReturnTemplateId()
        {
            var template = new Template
            {
                Name = "lendfoundry test template",
                Body = "Hello, {{CustomerName}}",
                Format = MessageFormat.Text,
            };

            var result = Catalog.Add(DataCreator.TenantId, template);
            Assert.Equal("lendfoundry-test-template", result);
        }
    }
}
