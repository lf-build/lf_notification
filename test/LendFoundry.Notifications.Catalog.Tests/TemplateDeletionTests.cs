﻿using LendFoundry.Notifications.Tests.Common;
using System;
using LendFoundry.Notifications.Abstractions;
using Xunit;

namespace LendFoundry.Notifications.Catalog.Tests
{
    public class TemplateDeletionTests
    {
        private ICatalog Catalog { get; set; }

        public TemplateDeletionTests()
        {
            Catalog = DataCreator.SetupTemplateCatalog();
        }

        [Fact]
        public void ShouldDeleteTemplate()
        {
            var id = DataCreator.TemplateId5;

            var result = Catalog.Delete(DataCreator.TenantId, id, MessageFormat.Text);
            Assert.True(result);
        }

        [Fact(Skip = "Need to fix")]
        public void ShouldThrowExceptionIfTenantNotFound()
        {
            Assert.Throws<Exception>(() => Catalog.Delete("InvalidTenant", DataCreator.TemplateId1, MessageFormat.Html));
        }

        [Fact(Skip = "Need to fix")]
        public void ShouldThrowExceptionIfTemplateIdNotFound()
        {
            Assert.Throws<Exception>(() => Catalog.Delete(DataCreator.TenantId, "InvalidTemplateId", MessageFormat.Html));
        }

        [Fact(Skip = "Need to fix")]
        public void ShouldThrowExceptionIfFormatNotFound()
        {
            Assert.Throws<Exception>(() => Catalog.Delete(DataCreator.TenantId, DataCreator.TemplateId2, MessageFormat.Html));
        }
    }
}
