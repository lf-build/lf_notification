﻿using LendFoundry.Notifications.Tests.Common;
using System.Linq;
using LendFoundry.Notifications.Abstractions;
using Xunit;

namespace LendFoundry.Notifications.Catalog.Tests
{
    public class TemplateRetrievalTests
    {
        private ICatalog Catalog { get; }

        public TemplateRetrievalTests()
        {
            Catalog = DataCreator.SetupTemplateCatalog();
        }

        [Fact]
        public void ShouldGetOneTemplate()
        {
            var template = Catalog.Get(DataCreator.TenantId, DataCreator.TemplateId1, MessageFormat.Html);
            Assert.NotNull(template);
        }

        [Fact]
        public void ShouldReturnNullForInvalidTemplateId()
        {
            var template = Catalog.Get(DataCreator.TenantId, "InvalidTemplateId", MessageFormat.Html);
            Assert.Null(template);
        }

        [Fact]
        public void ShouldReturnNullForValidTemplateIdAndInvalidFormat()
        {
            var template = Catalog.Get(DataCreator.TenantId, DataCreator.TemplateId1, MessageFormat.Text);
            Assert.Null(template);
        }

        [Fact]
        public void ShouldGetAllTemplates()
        {
            var templates = Catalog.GetAllTemplatesForTenant(DataCreator.TenantId);
            Assert.NotEqual(templates.Count(), 0);
        }

        [Fact]
        public void ShouldReturnTemplatesInAllFormatsForTemplateId()
        {
            var templates = Catalog.GetTemplatesById(DataCreator.TenantId, DataCreator.TemplateId3);
            Assert.Equal(templates.Count(), 2);
        }

        [Fact]
        public void ShouldReturnTemplateInSingleFormatForTemplateId()
        {
            var templates = Catalog.GetTemplatesById(DataCreator.TenantId, DataCreator.TemplateId1);
            Assert.Equal(templates.Count(), 1);
        }
    }
}
