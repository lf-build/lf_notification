﻿using LendFoundry.EventHub.Client;
using System.Threading.Tasks;
using System;

namespace LendFoundry.Notifications.Channels.SendGrid.Tests.Fake
{
    public class FakeEventHub : IEventHubClient
    {
        private Action<EventInfo> Handler { get; set; }

        public EventInfo EventInfo { get; set; }

        public Task<bool> Publish<T>(string eventName, T @event)
        {
            return new Task<bool>(() => true);
        }

        public void On(string eventName, Action<EventInfo> handler)
        {
            Handler = handler;
        }

        public void Start()
        {
            Handler.Invoke(EventInfo);
        }

        public void StartAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> Publish<T>(T @event)
        {
            throw new NotImplementedException();
        }
    }
}