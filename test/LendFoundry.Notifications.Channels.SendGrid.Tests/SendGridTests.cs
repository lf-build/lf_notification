﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Notifications.Channels.SendGrid.Tests.Fake;
using LendFoundry.Notifications.Events;
using LendFoundry.Security.Tokens;
using Moq;
using Xunit;

namespace LendFoundry.Notifications.Channels.SendGrid.Tests
{
    public class SendGridTests
    {
        private Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        private FakeEventHub FakeEventClient { get; set; } = new FakeEventHub();

        private Mock<IConfigurationServiceFactory<Configuration>> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory<Configuration>>();
        private Mock<IConfigurationService<Configuration>> ConfigurationService { get; } = new Mock<IConfigurationService<Configuration>>();

        private Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        private Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        private Configuration Configuration { get; } = new Configuration
        {
            ApiKey = "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",
            FromName = "LendFoundry Development",
            FromAddress = "development@lendfoundry.com"
        };

        private SendGridDeliveryChannel SendGrid => new SendGridDeliveryChannel
            (
                FakeEventClient, 
                ConfigurationFactory.Object,
                TokenHandler.Object,
                Logger.Object
            );


        [Fact]
        public void Start_When_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(Mock.Of<IToken>());
            ConfigurationService.Setup(x => x.Get()).Returns(Configuration);
            ConfigurationFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(Mock.Of<IConfigurationService<Configuration>>());
            
            FakeEventClient.EventInfo = new EventInfo
            {
                Data = new NotificationRequested
                {
                    Body = " BODY - UnitTest related to SendGridTests!",
                    Format = MessageFormat.Text,
                    Recipient = new Recipient
                    {
                        Email = "m.lourenco@cinq.com.br", //johndoe@lendfoundry.com
                        LoanReferenceNumber = "loan001",
                        Name = "John Doe"
                    },
                    Subject = "UnitTest related to SendGridTests!"
                },
                TenantId = "my-tenant",
                Name = nameof(NotificationRequested)
            };
            

            // act
            SendGrid.Start();

            // assert
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.Once());
            ConfigurationFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.Once);
        }

        [Fact]
        public void Start_When_ExecutionFailed()
        {
            // arrange
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(Mock.Of<IToken>());
            ConfigurationService.Setup(x => x.Get()).Returns(Configuration);
            ConfigurationFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(Mock.Of<IConfigurationService<Configuration>>());

            FakeEventClient.EventInfo = new EventInfo
            {
                Data = new NotificationRequested(),
                TenantId = "my-tenant",
                Name = nameof(NotificationRequested)
            };

            // act
            SendGrid.Start();

            // assert
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.Once());
            ConfigurationFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.Once);
        }
    }
}